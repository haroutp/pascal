﻿using System;
using System.Collections.Generic;

namespace Pascal
{
    class Program
    {
        static void Main(string[] args)
        {
            // IList<IList<int>> result = Generate(33);
            // foreach (var item in result)
            // {
            //     for (int i = 0; i < item.Count; i++)
            //     {
            //         System.Console.Write(item[i] + " ");
            //     }
            //     System.Console.WriteLine();
            // }
            IList<int> rowResult = GetRow(3);

            foreach (var item in rowResult)
            {
                System.Console.Write(item + " ");
            }
            System.Console.WriteLine();
            
        }

        public static IList<int> GetRow(int rowIndex) {
            IList<IList<int>> result = new List<IList<int>>();

            IList<int> first_row = new List<int>();

            first_row.Add(1);
            result.Add(first_row);

            for (int i = 1; i <= 33; i++)
            {
                IList<int> prevRow = result[i -1];
                IList<int> currentRowList = new List<int>();

                currentRowList.Add(1);
                for (int j = 1; j < i; j++)
                {
                    currentRowList.Add(prevRow[j - 1] + prevRow[j]);
                }
                currentRowList.Add(1);
                result.Add(currentRowList);
            }

            return result[rowIndex];
        }
        public static IList<IList<int>> Generate(int numRows) {
            
            IList<IList<int>> result = new List<IList<int>>();

            if(numRows == 0) return result;
            IList<int> first_row = new List<int>();

            first_row.Add(1);
            result.Add(first_row);

            for (int i = 1; i < numRows; i++)
            {
                IList<int> prevRow = result[i -1];
                IList<int> currentRowList = new List<int>();

                currentRowList.Add(1);
                for (int j = 1; j < i; j++)
                {
                    currentRowList.Add(prevRow[j - 1] + prevRow[j]);
                }
                currentRowList.Add(1);
                result.Add(currentRowList);
            }

            return result;
        }

        // public static List<List<int>> Generate(int numRows) {
            
        //     List<List<int>> result = new List<List<int>>();

        //     if(numRows == 0) return result;
        //     List<int> first_row = new List<int>();

        //     first_row.Add(1);
        //     result.Add(first_row);

        //     for (int i = 1; i < numRows; i++)
        //     {
        //         List<int> prevRow = result[i -1];
        //         List<int> currentRowList = new List<int>();

        //         currentRowList.Add(1);
        //         for (int j = 1; j < i; j++)
        //         {
        //             currentRowList.Add(prevRow[j - 1] + prevRow[j]);
        //         }
        //         currentRowList.Add(1);
        //         result.Add(currentRowList);
        //     }

        //     return result;
        // }
    }
}
